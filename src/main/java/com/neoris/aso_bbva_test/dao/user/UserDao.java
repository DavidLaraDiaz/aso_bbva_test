package com.neoris.aso_bbva_test.dao.user;


import com.neoris.aso_bbva_test.entity.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * UserDao JpaRepository extends CRUD for Entity
 *
 * @author David Lara
 */

public interface UserDao extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findFirstByEmailAndStatus(String email, Integer status);
    Optional<UserEntity> findFirstByEmail(String email);
    Optional<UserEntity> findFirstByIdentification(String identification);

    Page<UserEntity> findAllByStatus(Integer status, Pageable pageable);

}
