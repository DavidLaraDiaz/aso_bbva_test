package com.neoris.aso_bbva_test.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * this class is used to create the basic audit object for all tables in the database
 * @author David Lara
 */

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public class BasicEntity {

    @CreatedDate
    @Column( name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    @CreatedBy
    @JsonIgnore
    @Column( name = "created_by", nullable = false, updatable = false)
    private Integer createdBy;

    @JsonIgnore
    @Column( name = "updated_at")
    private Date updatedAt;

    @JsonIgnore
    @Column( name = "updated_by")
    private Integer updatedBy;

    @JsonIgnore
    @Column( name = "delete_at")
    private Date deleteAt;

    @JsonIgnore
    @Column( name = "delete_by")
    private Integer deleteBy;

    @NotNull
    @Column( name = "status")
    private Integer status;

    public BasicEntity() {
        this.createdAt = new Date();
        this.status = 1;
    }


}
