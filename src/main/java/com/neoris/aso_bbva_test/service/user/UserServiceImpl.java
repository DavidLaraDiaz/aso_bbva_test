package com.neoris.aso_bbva_test.service.user;


import com.neoris.aso_bbva_test.dao.user.UserDao;
import com.neoris.aso_bbva_test.entity.user.UserEntity;
import com.neoris.aso_bbva_test.exception.ErrorDetail;
import com.neoris.aso_bbva_test.exception.NeorisRuntimeException;
import com.neoris.aso_bbva_test.payload.user.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserDao userDao;
    @Autowired
    private MessageSource messageSource;

    private final String OBJECT_NAME = "USER";




    //register new user
    @Override
    public UserEntity save(UserForm form) {

        UserEntity user = new UserEntity();

        //validations
        user.setEmail(validUserEmail(form.getEmail(), null));
        user.setIdentification(validUserIdentification(form.getIdentification(), null));

        user.setIdentificationType(form.getIdentificationType());
        user.setName(form.getName());
        user.setLastname(form.getLastname());
        user.setPhone(form.getPhone());
        user.setRol(form.getRol());

        //TODO default created by 1
        user.setCreatedBy(1);

        //save and return
        return this.userDao.save(user);

    }

    //save user without validation
    @Override
    public UserEntity save(UserEntity user) {
        return this.userDao.save(user);
    }

    //update user
    @Override
    public UserEntity save(Integer userId, UserForm form) {
        //find user by id
        UserEntity user = findById(userId).get();

        //validations
        user.setEmail(validUserEmail(form.getEmail(), userId));
        user.setIdentification(validUserIdentification(form.getIdentification(), userId));

        user.setIdentificationType(form.getIdentificationType());
        user.setName(form.getName());
        user.setLastname(form.getLastname());
        user.setPhone(form.getPhone());
        user.setRol(form.getRol());

        //TODO default by 1
        user.setUpdatedBy(1);
        user.setUpdatedAt(new Date());

        //save and return
        return this.userDao.save(user);
    }

    @Override
    public Optional<UserEntity> findById(Integer userId) {
        Optional<UserEntity> userEntity = this.userDao.findById(userId);
        if(!userEntity.isPresent()){
            throw new NeorisRuntimeException(
                    messageSource.getMessage(
                            "general.error.notFound",
                            new String[]{OBJECT_NAME, userId.toString()},
                            new Locale("")
                    ),
                    new ErrorDetail("VS-"+OBJECT_NAME+"-003"));
        }
        return userEntity;
    }

    @Override
    public Optional<UserEntity> findByEmailAndStatus(String email, Integer status) {
        Optional<UserEntity> userEntity = this.userDao.findFirstByEmailAndStatus(email, status);
        if(!userEntity.isPresent()){
            throw new NeorisRuntimeException(
                    messageSource.getMessage(
                            "general.error.notFound.custom",
                            new String[]{OBJECT_NAME, "EMAIL", email},
                            new Locale("")
                    ),
                    new ErrorDetail("VS-"+OBJECT_NAME+"-004"));
        }
        return userEntity;
    }

    @Override
    public UserEntity toggleStatusById(Integer userId) {
        UserEntity userEntity = findById(userId).get();

        if (userEntity.getStatus().equals(1)){
            userEntity.setStatus(0);
        }else {
            userEntity.setStatus(1);
        }
        return this.userDao.save(userEntity);
    }

    @Override
    public List<UserEntity> getAll() {
        return this.userDao.findAll();
    }

    @Override
    public Page<UserEntity> getAllPaged(Pageable pageable) {
        return this.userDao.findAllByStatus(1, pageable);
    }



    private String validUserEmail(String userEmail, Integer userId){
        //change email to lower and delete spaces
        String email = userEmail.toLowerCase().trim();

        //here find User by email
        Optional<UserEntity> userEntity = this.userDao.findFirstByEmail(email);

        //if is a new user
        if (userId == null && !userEntity.isPresent()){
            return email;
        }
        //if is a update user
        else if (userId.equals(userEntity.get().getUserId())){
            return email;
        }

        //Error aready exist user with email
        throw new NeorisRuntimeException(
                    messageSource.getMessage(
                            "general.error.exist.email",
                            new String[]{OBJECT_NAME, email},
                            new Locale("")
                    ),
                    new ErrorDetail("VS-"+OBJECT_NAME+"-001"));


    }

    private String validUserIdentification(String identification, Integer userId){

        //valid if is null
        if (identification == null || identification.isEmpty()){
            return null;
        }

        //change identification delete spaces
        String id = identification.trim();

        //here find User by identification
        Optional<UserEntity> userEntity = this.userDao.findFirstByIdentification(id);

        //if is a new id
        if (!userEntity.isPresent()){
            return id;
        }
        //if is a update user
        else if (userId.equals(userEntity.get().getUserId())){
            return id;
        }

        //Error aready exist user with email
        throw new NeorisRuntimeException(
                messageSource.getMessage(
                        "general.error.exist.identification",
                        new String[]{OBJECT_NAME, id},
                        new Locale("")
                ),
                new ErrorDetail("VS-"+OBJECT_NAME+"-002"));


    }

}
