package com.neoris.aso_bbva_test.exception;

public class NeorisRuntimeException extends RuntimeException{
    private static final long serialVersionUID = -7320416484106232920L;
    private ErrorDetail detail;

    public NeorisRuntimeException(String message, ErrorDetail detail) {
        super(message);
        this.detail = detail;
    }

    public NeorisRuntimeException(String message, Throwable cause, ErrorDetail detail) {
        super(message, cause);
        this.detail = detail;
    }

    public ErrorDetail getDetail() {
        return this.detail;
    }
}
