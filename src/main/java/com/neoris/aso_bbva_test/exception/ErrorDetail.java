package com.neoris.aso_bbva_test.exception;

import java.util.Arrays;

public class ErrorDetail {

    private final Object code;
    private String source;
    private String detail;
    private Object[] params;
    private boolean beanValidation;
    private Object errors;

    public ErrorDetail(Object code, String source, String detail, Object... params) {
        this.code = code;
        this.source = source;
        this.detail = detail;
        this.params = params;
    }

    public ErrorDetail(Object code, boolean beanValidation, Object errors) {
        this.code = code;
        this.beanValidation = beanValidation;
        this.errors = errors;
    }

    public Object[] getParams() {
        return this.params != null ? Arrays.copyOf(this.params, this.params.length) : null;
    }

    public Object getCode() {
        return this.code;
    }

    public String getSource() {
        return this.source;
    }

    public String getDetail() {
        return this.detail;
    }

    public boolean isBeanValidation() {
        return this.beanValidation;
    }

    public Object getErrors() {
        return this.errors;
    }

    public ErrorDetail(Object code) {
        this.code = code;
    }
}
