package com.neoris.aso_bbva_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsoBbvaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsoBbvaTestApplication.class, args);
	}

}
