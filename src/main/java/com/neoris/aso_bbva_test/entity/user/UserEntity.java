package com.neoris.aso_bbva_test.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.neoris.aso_bbva_test.enums.user.UserIdentificationEnum;
import com.neoris.aso_bbva_test.utils.BasicEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This Class is a representation tha table user in the data base
 * @author David Lara
 */

@Entity
@Table(name = "tab_user")
@Data
@org.hibernate.annotations.Table(appliesTo = "tab_user", comment = "Tabla Usuarios neoris")
public class UserEntity extends BasicEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "name")
    private String name;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "identification_type")
    @Enumerated(EnumType.STRING)
    private UserIdentificationEnum identificationType;

    @Column(name = "identification", unique = true)
    private String identification;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "rol")
    private String rol;

    @JsonIgnore
    @Column(name = "password")
    private String password;


    public UserEntity() {
    }

    public UserEntity(String name, String lastname, UserIdentificationEnum identificationType, String identification, String email) {
        this.name = name;
        this.lastname = lastname;
        this.identificationType = identificationType;
        this.identification = identification;
        this.email = email;
    }
}
