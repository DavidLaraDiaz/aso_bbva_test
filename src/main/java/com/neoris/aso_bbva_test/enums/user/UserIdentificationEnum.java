package com.neoris.aso_bbva_test.enums.user;

public enum UserIdentificationEnum {
    CEDULA,TARJETA_DE_IDENTIDAD, PASAPORTE
}
