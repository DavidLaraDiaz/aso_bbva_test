                Prueba técnica BBVA
Realizar un webservice que contenga las 4 operaciones básicas(GET, POST, PUT Y DELETE) de unservicio REST por medio del uso de:


  Carpeta 1 del artefacto de software

●Java 1.8
●Framework Springboot
●Maven como gestor de dependencias
●Arquitectura de 3 capas o MVC (o patrón similar)
●Git y la nube de repositorio de código a libre elección
●Proyecto de Postman o plataforma de prueba-uso de APIS


  Carpeta 2

●Esquema de bases de datos o gestión de backend para probar desarrollo
  
  
  Carpeta 3

●Explicación de la solución con diagrama de alto nivelPlus
●Backend y API en la nube para prueba por postman directa
●Buenas prácticas de programación
●Test Unitarios


  Nota: El participante podrá elegir el tema que desee para construir el webservices.


        Deploy

        -Verificar el puerto 8002 para el despliegue, si es necesario cambiarlo en application.properties
        -Ejecutar con un IDE compatible maven



Documentacion y uso de API : http://localhost:8002/swagger-ui.html